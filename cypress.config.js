const { defineConfig } = require("cypress");

module.exports = defineConfig({
  projectId: 'g4ovvi',
  reporter: 'cypress-mochawesome-reporter',
  e2e: {

    watchForFileChanges: false,
    setupNodeEvents(on, config) {
      require('cypress-mochawesome-reporter/plugin')(on);
      // implement node event listeners here
    },
  },
});