export class BasePage {
  url = "https://opensource-demo.orangehrmlive.com/web/index.php/auth/login";

  loadUrl() {
    cy.visit(this.url);
  }

  waitForSync() {
    cy.wait(1000);
  }

  setViewPortTo1280X720() {
    cy.viewport(1280, 720); // setting the viewport (width height) to get full screen
  }
}
