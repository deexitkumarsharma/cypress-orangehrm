# Cypress-OrangeHRM

## Run Tests

To run cart actions tests in interactive mode use following command

`npx cypress open`

## Demo

![App Screenshot](/cypress/screenshots/Screenshot.png)

## Patterns

Despite what Cypress says, the page object model is used because it improves code readability.

- Fixtures > loginDetails.json provides data.

## Dependencies

    "cypress": "^12.4.0",
    "cypress-mochawesome-reporter": "^3.2.3"

## Feedback

If you have any feedback, please reach out to us at deexitkumarsharma@gmail.com
